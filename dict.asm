global find_word

extern string_length
extern string_equals

find_word:
    push rsi
    push rdi
    add rsi, 8
    call string_equals
    pop rdi
    pop rsi
    test rax, rax       
    jnz .found
    mov rsi, [rsi]
    test rsi, rsi
    jnz find_word
    xor rax, rax
    ret   
.found:
    mov rax, rsi
    ret                  
