global _start

section .rodata

long_word: db 'Word length should be less than 256', 0xA, 0
not_found: db 'There is no such word in dictionary', 0xA, 0

section .data

%include "lib.inc"
%include "colon.inc"
%include "dict.inc"
%include "words.inc"
section .text
global _start 


_start:
        call read_key
        push rdx
        mov rdi, rax
        call find_key
        pop rdx
        add rax, 8
        add rax, rdx
        inc rax
        mov rdi, rax
        call print_string
        call print_newline
        xor rdi, rdi
        jmp exit
read_key:
        mov rsi, 256
        mov rdi, rsp
        sub rdi, rsi
        call read_line
        cmp rax, 0
        je long_word
        ret
find_key:
        mov rsi, pointer
        call find_word
        cmp rax, 0
        je not_found
        ret
not_found:
        mov rdi, not_found
        jmp bad_print
long_word:
        mov rdi, long_word
bad_print:
        call print_string
        xor rdi, rdi
        jmp exit
